Send Info To Splunk
=========

Sends the information of workflow tower jobs that failed to Splunk.

Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

| Variable                | Required | Default | Choices                   | Comments                                 |
|-------------------------|----------|---------|---------------------------|------------------------------------------|
| tower_username          | yes      | -    | -                         | -                         |
| tower_password          | yes      | -    | -                         | -                         |
| tower_url               | yes      | tower-cloudlet.cloudlet-dev.com    | -                         | -                         |
| splunk_collector_url    | yes      | [https://13.90.23.80:8088/services/collector/event](https://13.90.23.80:8088/services/collector/event)    | -                         | -                         |
| splunk_token    | yes      | -    | -                         | Splunk 1111-1111-1111-1111                        |

Dependencies
------------

- [Role Send To Splunk](http://gitlab.cloudlet-dev.com/cloudlet/role-send-to-splunk.git)

Example
----------------

```yaml
- hosts: all
  roles:
  - role: send_info_to_splunk
    vars:
      tower_username: 'USERNAME'
      tower_password: 'PASSWORD'
      splunk_token: 'Splunk 1111-1111-1111-1111'
```
